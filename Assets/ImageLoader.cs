﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ImageLoader : MonoBehaviour
{
    // Start is called before the first frame update
    public TMP_InputField inputField;

    public Image testImage;

    void Start()
    {
                
    }


    /// <summary>
    /// Překonvertuje stringovou reprezentaci obrázku na Texturu a
    /// přiřadí ji objektu.
    /// </summary>
    public void ConvertTexture()
    {
        byte[] data = File.ReadAllBytes(inputField.text);

        Texture2D tex = new Texture2D(100, 100);
        //obrázek nemusí být přiřezený v databázi
        if (data != null && data.Length > 0)
        {
            tex.LoadImage(data);

        //Sprite personSprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
            Sprite av = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f));

            // RawImage image = employeePanel.transform.Find("Image").GetComponent<RawImage>();

            testImage.sprite = av;
            testImage.type = Image.Type.Simple;
            testImage.preserveAspect = true;
        }

    }
}
